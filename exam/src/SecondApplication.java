public class SecondApplication {
    
    class Base {
        public float n;

        public void f(int g) {

        }

        class L extends Base {
            private long t;
            private M m;

            public void f() {

            }
        }

        class X {
            public void i(L l) {

            }
        }
    }

    class M {
    }

    class A {
        private M m;

        public void metA() {

        }
    }

    class B {
        private M m;

        public void metB() {

        }
    }
}
