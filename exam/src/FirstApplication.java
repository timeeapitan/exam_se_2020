public class FirstApplication extends Thread {

    FirstApplication(String name) {
        super(name);
    }

    @Override
    public void run() {
        for (int i = 0; i < 2; i++) { //6 messages in the console, 2 messages per thread
            System.out.println("[" + Thread.currentThread().getName() + "]-[" + System.currentTimeMillis() + "]");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        FirstApplication t1 = new FirstApplication("KThread1");
        FirstApplication t2 = new FirstApplication("KThread2");
        FirstApplication t3 = new FirstApplication("KThread3");
        t1.start();
        t2.start();
        t3.start();
    }
}
